package models

import (
	"time"
)

type CreateTicketReq struct {
	Title       string    `json:"title"`
	Description string    `json:"description"`
	Contact     string    `json:"contact"`
	Information string    `json:"information"`
	CreatedTime time.Time `json:"created_timestamp"`
	UpdatedTime time.Time `json:"latest_ticket_update_timestamp"`
}

type CreateTicketRes struct {
	ID          string `json:"id" db:"id"`
	Title       string `json:"title" db:"title"`
	Description string `json:"description" db:"description"`
	Contact     string `json:"contact" db:"contact"`
	Information string `json:"information" db:"information"`
	CreatedTime string `json:"created_timestamp" db:"created_timestamp"`
	UpdatedTime string `json:"latest_ticket_update_timestamp" db:"latest_ticket_update_timestamp"`
	Status      string `json:"status" db:"status"`
}

type UpdateStatusReq struct {
	ID     string `json:"id"`
	Status string `json:"status"`
}

type Filter struct {
	Page     int    `json:"page"`
	Per_Page int    `json:"per_page"`
	Status   string `json:"status"`
	Order    string `json:"order"`
}

type TicketList struct {
	TotalItems int                `json:"total_items"`
	Page       int                `json:"page"`
	PerPage    int                `json:"per_page"`
	Lists      []*CreateTicketRes `json:"list"`
}

type TicketListARR []CreateTicketRes
