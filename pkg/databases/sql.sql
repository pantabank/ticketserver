CREATE TYPE status AS ENUM ('pending', 'accepted', 'resolved', 'rejected');

CREATE SEQUENCE ticket_sequence
    START WITH 1001
    INCREMENT BY 1;

CREATE TABLE "tickets" (
    "id"    VARCHAR(255) PRIMARY KEY NOT null,
    "title" VARCHAR(255),
    "description"   VARCHAR(255),
    "contact"   VARCHAR(255),
    "information"   VARCHAR(255),
    "created_timestamp" TIMESTAMP,
    "latest_ticket_update_timestamp"    TIMESTAMP, 
    "status"    status
);