package usecases

import (
	"github.com/pantabank/ticket/models"
	"github.com/pantabank/ticket/modules/entities"
)

type ticketUse struct {
	TicketRepo entities.TicketRepository
}

func NewTicketUsecase(ticketRepo entities.TicketRepository) entities.TicketUsecase {
	return &ticketUse{
		TicketRepo: ticketRepo,
	}
}

func (u *ticketUse) CreateTicket(req *models.CreateTicketReq) (*models.TicketList, error) {

	_, err := u.TicketRepo.CreateTicket(req)
	if err != nil {
		return nil, err
	}

	res, err := u.TicketList(&models.Filter{})
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (u *ticketUse) UpdateTicketStatus(req *models.TicketListARR) (*models.TicketList, error) {

	for _, r := range *req {
		_, err := u.TicketRepo.UpdateTicketStatus(&r)
		if err != nil {
			return nil, err
		}
	}

	res, err := u.TicketList(&models.Filter{})
	if err != nil {
		return nil, err
	}

	return res, err
}

func (u *ticketUse) TicketList(filter *models.Filter) (*models.TicketList, error) {

	list := new(models.TicketList)

	res, err := u.TicketRepo.TicketList(filter)
	if err != nil {
		return nil, err
	}

	count, err := u.TicketRepo.TicketListCount(filter)
	if err != nil {
		return nil, err
	}

	list.TotalItems = count
	list.Page = filter.Page
	list.PerPage = filter.Per_Page
	list.Lists = res

	if list.Lists == nil {
		list.Lists = []*models.CreateTicketRes{}
	}

	return list, err
}
