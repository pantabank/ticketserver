package controllers

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
	"github.com/pantabank/ticket/models"
	"github.com/pantabank/ticket/modules/entities"
)

type ticketController struct {
	TicketUse entities.TicketUsecase
}

func NewTicketController(r fiber.Router, ticketUse entities.TicketUsecase) {
	controllers := &ticketController{
		TicketUse: ticketUse,
	}
	r.Post("/", controllers.CreateTicket)
	r.Patch("/", controllers.UpdateTicketStatus)
	r.Get("/", controllers.TicketList)
}

func (h *ticketController) CreateTicket(c *fiber.Ctx) error {
	req := new(models.CreateTicketReq)
	if err := c.BodyParser(req); err != nil {
		return c.Status(fiber.ErrBadRequest.Code).JSON(fiber.Map{
			"status":      fiber.ErrBadRequest.Message,
			"status_code": fiber.ErrBadRequest.Code,
			"message":     err.Error(),
			"result":      nil,
		})
	}

	res, err := h.TicketUse.CreateTicket(req)
	if err != nil {
		return c.Status(fiber.ErrInternalServerError.Code).JSON(fiber.Map{
			"status":      fiber.ErrInternalServerError.Message,
			"status_code": fiber.ErrInternalServerError.Code,
			"message":     err.Error(),
			"result":      nil,
		})
	}

	return c.Status(fiber.StatusOK).JSON(fiber.Map{
		"status":      "OK",
		"status_code": fiber.StatusOK,
		"message":     "",
		"result":      res,
	})
}

func (h *ticketController) UpdateTicketStatus(c *fiber.Ctx) error {
	req := new(models.TicketListARR)
	if err := c.BodyParser(req); err != nil {
		return c.Status(fiber.ErrBadRequest.Code).JSON(fiber.Map{
			"status":      fiber.ErrBadRequest.Message,
			"status_code": fiber.ErrBadRequest.Code,
			"message":     err.Error(),
			"result":      nil,
		})
	}

	fmt.Println(req)

	res, err := h.TicketUse.UpdateTicketStatus(req)
	if err != nil {
		return c.Status(fiber.ErrInternalServerError.Code).JSON(fiber.Map{
			"status":      fiber.ErrInternalServerError.Message,
			"status_code": fiber.ErrInternalServerError.Code,
			"message":     err.Error(),
			"result":      nil,
		})
	}

	return c.Status(fiber.StatusOK).JSON(fiber.Map{
		"status":      "OK",
		"status_code": fiber.StatusOK,
		"message":     "",
		"result":      res,
	})
}

func (h *ticketController) TicketList(c *fiber.Ctx) error {
	filter := new(models.Filter)
	if err := c.QueryParser(filter); err != nil {
		return c.Status(fiber.ErrBadRequest.Code).JSON(fiber.Map{
			"status":      fiber.ErrBadRequest.Message,
			"status_code": fiber.ErrBadRequest.Code,
			"message":     err.Error(),
			"result":      nil,
		})
	}

	if filter.Status == "All" {
		filter.Status = ""
	}

	res, err := h.TicketUse.TicketList(filter)
	if err != nil {
		return c.Status(fiber.ErrInternalServerError.Code).JSON(fiber.Map{
			"status":      fiber.ErrInternalServerError.Message,
			"status_code": fiber.ErrInternalServerError.Code,
			"message":     err.Error(),
			"result":      nil,
		})
	}

	return c.Status(fiber.StatusOK).JSON(fiber.Map{
		"status":      "OK",
		"status_code": fiber.StatusOK,
		"message":     "",
		"result":      res,
	})
}
