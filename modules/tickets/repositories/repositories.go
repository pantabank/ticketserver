package repositories

import (
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/pantabank/ticket/models"
	"github.com/pantabank/ticket/modules/entities"
)

type ticketRepo struct {
	Db *sqlx.DB
}

func NewTicketRepository(db *sqlx.DB) entities.TicketRepository {
	return &ticketRepo{
		Db: db,
	}
}

func (r *ticketRepo) CreateTicket(req *models.CreateTicketReq) (*models.CreateTicketRes, error) {

	query := `
		INSERT INTO "tickets" (
			"id",
			"title",
			"description",
			"contact",
			"information",
			"created_timestamp",
			"latest_ticket_update_timestamp",
			"status"
		)
		VALUES (concat('T', nextval('ticket_sequence')), $1, $2, $3, $4, $5, $6, 'pending')
		RETURNING "title", "description", "contact", "information", "created_timestamp", "latest_ticket_update_timestamp", "status"
	`

	ticket := new(models.CreateTicketRes)

	rows, err := r.Db.Queryx(query, req.Title, req.Description, req.Contact, req.Information, time.Now(), time.Now())
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	for rows.Next() {
		if err := rows.StructScan(ticket); err != nil {
			fmt.Println(err.Error())
			return nil, err
		}
	}

	defer rows.Close()

	return ticket, nil
}

func (r *ticketRepo) UpdateTicketStatus(req *models.CreateTicketRes) (*models.CreateTicketRes, error) {

	query := `
		UPDATE "tickets" SET
			status = $1
		WHERE id = $2
	`

	_, err := r.Db.Query(query, req.Status, req.ID)
	if err != nil {
		return nil, err
	}

	return nil, err
}

func (r *ticketRepo) TicketList(filter *models.Filter) ([]*models.CreateTicketRes, error) {
	query := `
		SELECT 
			id,
			title,
			description,
			contact,
			information,
			created_timestamp,
			latest_ticket_update_timestamp,
			status
		FROM tickets
		WHERE 1=1 `

	if filter.Status != "" {
		query += ` AND status = '` + filter.Status + `'`
	}

	if filter.Order == "" {
		filter.Order = "latest_ticket_update_timestamp"
	}

	query += ` ORDER BY ` + filter.Order + ` DESC`

	pages := filter.Per_Page * (filter.Page - 1)

	if filter.Page != 0 && filter.Per_Page != 0 {
		query += fmt.Sprintf(` LIMIT %d OFFSET %d`, filter.Per_Page, pages)
	}

	var list []*models.CreateTicketRes

	rows, err := r.Db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		Query := new(models.CreateTicketRes)
		err := rows.Scan(&Query.ID, &Query.Title, &Query.Description, &Query.Contact, &Query.Information, &Query.CreatedTime, &Query.UpdatedTime, &Query.Status)
		if err != nil {
			return nil, err
		}
		list = append(list, Query)
	}

	return list, nil
}

func (r *ticketRepo) TicketListCount(filter *models.Filter) (int, error) {
	query := `
		SELECT 
			COUNT(*)
		FROM tickets
		WHERE 1=1 `

	if filter.Status != "" {
		query += ` AND status = '` + filter.Status + `'`
	}

	var count int
	if err := r.Db.QueryRow(query).Scan(&count); err != nil {
		return 0, err
	}

	return count, nil
}
