package entities

import "github.com/pantabank/ticket/models"

type TicketUsecase interface {
	CreateTicket(req *models.CreateTicketReq) (*models.TicketList, error)
	UpdateTicketStatus(req *models.TicketListARR) (*models.TicketList, error)
	TicketList(filter *models.Filter) (*models.TicketList, error)
}

type TicketRepository interface {
	CreateTicket(req *models.CreateTicketReq) (*models.CreateTicketRes, error)
	UpdateTicketStatus(req *models.CreateTicketRes) (*models.CreateTicketRes, error)
	TicketList(filter *models.Filter) ([]*models.CreateTicketRes, error)
	TicketListCount(filter *models.Filter) (int, error)
}
