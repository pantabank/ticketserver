package servers

import (
	_ticketHttp "github.com/pantabank/ticket/modules/tickets/controllers"
	_ticketRepository "github.com/pantabank/ticket/modules/tickets/repositories"
	_ticketUsecase "github.com/pantabank/ticket/modules/tickets/usecases"

	"github.com/gofiber/fiber/v2"
)

func (s *Server) MapHandlers() error {
	v1 := s.App.Group("/v1")

	ticketGroup := v1.Group("/ticket")
	ticketRepository := _ticketRepository.NewTicketRepository(s.Db)
	TicketUsecase := _ticketUsecase.NewTicketUsecase(ticketRepository)
	_ticketHttp.NewTicketController(ticketGroup, TicketUsecase)

	s.App.Use(func(c *fiber.Ctx) error {
		return c.Status(fiber.ErrInternalServerError.Code).JSON(fiber.Map{
			"status":      fiber.ErrInternalServerError.Message,
			"status_code": fiber.ErrInternalServerError.Code,
			"message":     "error, end point not found",
			"result":      nil,
		})
	})

	return nil
}
