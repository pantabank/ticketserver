package servers

import (
	"log"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors" // เพิ่มการนำเข้าโมดูล cors
	"github.com/jmoiron/sqlx"
	"github.com/pantabank/ticket/configs"
	"github.com/pantabank/ticket/pkg/utils"
)

type Server struct {
	App *fiber.App
	Cfg *configs.Configs
	Db  *sqlx.DB
}

func NewServer(cfg *configs.Configs, db *sqlx.DB) *Server {
	app := fiber.New()

	// ตั้งค่า CORS
	app.Use(cors.New(cors.Config{
		AllowOrigins: "*", // กำหนดโดเมนที่อนุญาตให้เข้าถึง API
	}))

	return &Server{
		App: app,
		Cfg: cfg,
		Db:  db,
	}
}

func (s *Server) Start() {
	if err := s.MapHandlers(); err != nil {
		log.Fatalln(err.Error())
		panic(err.Error())
	}

	fiberConnURL, err := utils.ConnectionUrlBuilder("fiber", s.Cfg)
	if err != nil {
		log.Fatalln(err.Error())
		panic(err.Error())
	}

	host := s.Cfg.App.Host
	port := s.Cfg.App.Port
	log.Printf("server has been started on %s:%s ⚡", host, port)

	if err := s.App.Listen(fiberConnURL); err != nil {
		log.Fatalln(err.Error())
		panic(err.Error())
	}
}
